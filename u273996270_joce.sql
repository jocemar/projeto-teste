
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 25/06/2017 às 20:28:10
-- Versão do Servidor: 10.1.24-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u273996270_joce`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro`
--

CREATE TABLE IF NOT EXISTS `cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL DEFAULT '',
  `cpf` varchar(50) NOT NULL,
  `rg` varchar(50) DEFAULT NULL,
  `estado` varchar(100) NOT NULL,
  `nascimento` date NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `hora_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Extraindo dados da tabela `cadastro`
--

INSERT INTO `cadastro` (`id`, `nome`, `cpf`, `rg`, `estado`, `nascimento`, `telefone`, `hora_registro`) VALUES
(112, 'Jocemar Guilherme Weber', '193.890.183-09', '', 'pr', '1989-10-25', '(47) 8888-8888', '2017-06-25 20:19:01'),
(111, 'Carol', '090.898.989-89', '7.878.787', 'sc', '1989-10-24', '(47) 8801-1083', '2017-06-25 20:17:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro-telefone`
--

CREATE TABLE IF NOT EXISTS `cadastro-telefone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idfone` int(11) NOT NULL,
  `telefoneextra` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `cadastro-telefone`
--

INSERT INTO `cadastro-telefone` (`id`, `idfone`, `telefoneextra`) VALUES
(24, 112, '(47) 88888-8999');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
