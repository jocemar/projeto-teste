$(function() {
    var currentIndex = 0;
    $('#adicionar-inscricao').on('click', function(e) {
        e.preventDefault();
        $('#telefoneextra').append('<div class="form-group col-md-6"><input type="telefone" name="telefoneextra[]" class="form-control mask-phone" placeholder="Telefone Extra"></div>')
    });

    /* Mascara */
    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.mask-phone').mask(SPMaskBehavior, spOptions);
    $('.mask-nasc').mask('00/00/0000');
    $('.mask-cpf').mask('000.000.000-00');
    $('.mask-rg').mask('0.000.000');


    $( "#estado" ).on( "change", function() {
        if(this.value == 'sc'){
            $( "#rg").fadeIn();
        }else {
            $( "#rg").fadeOut();
        }
    });

    $('#nasc').change(function(){
        var estado = $('#estado').val();
        if(estado == 'pr'){
            var nascimento = this.value.split("/");
            var dataNascimento = new Date(parseInt(nascimento[2], 10),
                parseInt(nascimento[1], 10) - 1,
                parseInt(nascimento[0], 10));

            var diferenca = Date.now() -  dataNascimento.getTime();
            var idade = new Date(diferenca);

            var idade = Math.abs(idade.getUTCFullYear() - 1970);
            if (idade <= 18){
                alert('Você precisa de mais de 18 anos para realizar este cadastro');
                $('.bt-cadastro').prop('disabled', true);
            }
        }
    });


    //Efeito Relatório:
    (function(){
        'use strict';
        var $ = jQuery;
        $.fn.extend({
            filterTable: function(){
                return this.each(function(){
                    $(this).on('keyup', function(e){
                        $('.filterTable_no_results').remove();
                        var $this = $(this),
                            search = $this.val().toLowerCase(),
                            target = $this.attr('data-filters'),
                            $target = $(target),
                            $rows = $target.find('tbody tr');

                        if(search == '') {
                            $rows.show();
                        } else {
                            $rows.each(function(){
                                var $this = $(this);
                                $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                            })
                            if($target.find('tbody tr:visible').size() === 0) {
                                var col_count = $target.find('tr').first().find('td').size();
                                var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
                                $target.find('tbody').append(no_results);
                            }
                        }
                    });
                });
            }
        });
        $('[data-action="filter"]').filterTable();
    })(jQuery);

    $(function(){
        // attach table filter plugin to inputs
        $('[data-action="filter"]').filterTable();

        $('.container').on('click', '.panel-heading span.filter', function(e){
            var $this = $(this),
                $panel = $this.parents('.panel');

            $panel.find('.panel-body').slideToggle();
            if($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
    })


});

