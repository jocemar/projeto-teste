<meta name="viewport" content="width=1024">
<div class="container">
    <h1>Registros Cadastro</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <form action="/projeto/relatorio" method="post">
                        <select class="form-control" name="busca">
                            <option>Buscar por:</option>
                            <option value="nome">Nome</option>
                            <option value="hora_registro">Data de Cadastro</option>
                            <option value="nascimento">Data de Nascimento</option>
                        </select>
                        <button type="submit" class="btn btn-default">Filtrar</button>
                    </form>
                </div>
                <div class="panel-body">
                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Developers" />
                </div>
                <table class="table table-hover" id="dev-table">
                    <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>RG</th>
                        <th>Nascimento</th>
                        <th>Telefone</th>
                        <th>Telefone Extra</th>
                        <th>Data de Cadastro</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($registros as $r){?>
                        <tr>
                            <td><?= $r->estado ?></td>
                            <td><?= $r->nome ?></td>
                            <td><?= $r->cpf ?></td>
                            <td><?= $r->rg ?></td>
                            <td><?=  date("d/m/Y", strtotime($r->nascimento)) ?></td>
                            <td><?= $r->telefone ?></td>
                            <td><?= $r->telefoneextra ?></td>
                            <td><?= $r->hora_registro ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



