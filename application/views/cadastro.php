<section id="contato">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">

                <?php
                $verificar = $this->uri->segment(1);
                if( $verificar != NULL):?>

                    <div class="col-md-12 alert">
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            Seu cadastro foi enviado. Obrigado!
                        </div>
                    </div>

                    <?php
                endif;
                ?>

                <div class="form-area">
                    <form class="form-cadastro" action="/projeto/cadastro/cadastrar/" method="post">
                        <br style="clear:both">
                        <h2 class="section-heading">faça seu <strong>CADASTRO</strong></h2>
                            <select class="form-control" id="estado" name="estado">
                                <option>SELECIONE O ESTADO:</option>
                                <option value="sc">Santa Catarina</option>
                                <option value="pr">Paraná</option>
                            </select>
                        <div class="form-group">
                            <input type="nome" name="nome" class="form-control" placeholder="Nome" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="cpf" name="cpf" class="form-control mask-cpf" placeholder="CPF" required>
                        </div>
                        <div class="form-group col-md-6"id="rg">
                            <input type="rg" name="rg" class="form-control mask-rg" placeholder="RG">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="nascimento" name="nascimento" class="form-control mask-nasc" id="nasc" placeholder="Nascimento" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="telefone" name="telefone" class="form-control mask-phone" placeholder="Telefone" required>
                        </div>
                        <div class="form-group col-md-6">
                            <a href="#" id="adicionar-inscricao">Adicionar novo <strong>Telefone</strong></a>
                        </div>

                        <div id="telefoneextra"></div>

                        <!--<button type="button" id="submit" name="submit" class="btn btn-primary pull-right">Cadastrar</button>-->
                        <button type="submit" class="btn btn-primary pull-right bt-cadastro">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

