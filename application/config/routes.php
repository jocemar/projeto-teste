<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'cadastro';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['projeto/cadastrar'] = 'cadastro/cadastrar/';
$route['relatorio'] = 'relatorio';
$route['cadastro'] = 'cadastro';




