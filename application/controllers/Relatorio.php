<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio extends CI_Controller {

    public function index()
    {

        $data = array();
        $this->load->model('Cadastro_model');
        $busca = $this->input->post('busca');
        $data['registros'] = $this->Cadastro_model->selecionar($busca);

        $this->load->view('header');
        $this->load->view('relatorio', $data);
        $this->load->view('footer');
    }
}
