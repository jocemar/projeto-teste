<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller {

    public function index()
    {
        $this->load->view('header');
        $this->load->view('cadastro');
        $this->load->view('footer');
    }

    public function cadastrar()
    {
        $data = array();
        $this->load->model('cadastro_model');
        $this->cadastro_model->inserir();

        redirect('cadastro');
    }
}
