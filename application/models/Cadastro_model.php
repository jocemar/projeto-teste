<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro_model extends CI_Model{

    public $nome = "";
    public $cpf = "";
    public $nascimento = "";
    public $telefone = "";
    public $rg = "";

    function inserir()
    {
        try {
            $this->nome        = $this->input->post('nome');
            $this->cpf         = $this->input->post('cpf');
            $this->telefone    = $this->input->post('telefone');
            $this->nascimento  = $this->input->post('nascimento');
            $this->rg          = $this->input->post('rg');
            $this->estado      = $this->input->post('estado');

            //converte data Nascimento:
            $this->nascimento = implode('-', array_reverse(explode('/', $this->nascimento)));

            //insere registro:
            $this->db->insert('cadastro', $this);
            $id = $this->db->insert_id();

            //Se tiver mais telefones, salva aqui:
            $registro['telefoneextra'] = $this->input->post('telefoneextra');
            if(  $registro['telefoneextra'] != NULL ) {
                foreach ($registro['telefoneextra'] as $fone):
                    $registro['idfone'] = $id;
                    $registro['telefoneextra'] = $fone;
                    $this->db->insert('cadastro-telefone', $registro);
                endforeach;
            }

                return $id;

        }catch (Exception $ex){
            throw new Exception("Erro ao salvar no banco de dados ->" .$ex->getMessage());
        }
    }

    //busca registros:
    public function selecionar($busca = NULL){

        //ordenação:
        if ($busca == 'nome'){
            $ordem = 'ASC';
        }elseif ($busca == 'hora_registro'){
            $ordem = 'DESC';
        }elseif ($busca == 'nascimento'){
            $ordem = 'DESC';
        }else{
            $busca = 'cadastro.id';
            $ordem = 'DESC';
        }

        $this->db->select('*');
        $this->db->from('cadastro');
        $this->db->join('cadastro-telefone', 'cadastro.id = cadastro-telefone.idfone', 'left');
        $this->db->order_by($busca, $ordem);
        $query = $this->db->get();
        return $query->result();
    }
}